package com.bbva.segundaactividad

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class RegistroActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        val btnRegistrar=findViewById<Button>(R.id.btnRegistrar)

        btnRegistrar.setOnClickListener{
            val intent= Intent(this, OlvidePassActivity::class.java)
            startActivity(intent)
        }
    }
}