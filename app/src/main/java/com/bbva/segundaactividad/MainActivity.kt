package com.bbva.segundaactividad

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnIniciar=findViewById<Button>(R.id.btnIniciar)

        btnIniciar.setOnClickListener{
            val intent= Intent(this, RegistroActivity::class.java)
            startActivity(intent)
        }
    }
}