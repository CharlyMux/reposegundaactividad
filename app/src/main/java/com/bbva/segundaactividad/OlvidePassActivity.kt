package com.bbva.segundaactividad

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class OlvidePassActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_olvide_pass)

        val btnRecuperar=findViewById<Button>(R.id.btnRecuperar)

        btnRecuperar.setOnClickListener{
            val intent= Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }
    }
}